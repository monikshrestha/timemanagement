<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/Login.css" />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
      rel="stylesheet"
    />
    <title>Document</title>
  </head>
  <body>
    <div class="Login">
      <div class="row">
        <div class="col">
          <div class="container">
            <div class="page-informaton">
              <h4>Welcome to taskmanangement <br>system</h4>
              <p>We are here to improve and record all of your daily task</p>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="login-wrapper">
            <div class="logincard">
              <form>
                <div class="formlabel">
                  <p>LOGIN PANEL</p>
                </div>
                <div class="form-group">
                  <i class="fas fa-user"></i
                  ><label for="email" class="pl-2 font-weight-bold">Email</label
                  ><input
                    type="text"
                    class="form-control"
                    placeholder="Email"
                    name="rEmail"
                  />
                </div>
                <div class="form-group">
                  <i class="fas fa-key"></i
                  ><label for="pass" class="pl-2 font-weight-bold"
                    >Password</label
                  ><input
                    type="text"
                    class="form-control"
                    placeholder="Password"
                    name="rPassword"
                  />
                </div>
                <input
                  type="submit"
                  value="Login"
                  class="btn btn-outline-success mt-3 shadow-sm font-weight-bold"
                />
                <div class="signup">
                  Dont have an account?
                  <span>
                    <a href="MainPage.html"> Signup </a>
                  </span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
