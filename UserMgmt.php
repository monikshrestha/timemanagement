<?php require_once 'Header.php' ?>

<div class="row body">
    <div class="col-2">
        <?php require_once 'Sidebar.php' ?>
    </div>
    <div class="col-10">
        <div class="mainbody">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-User-tab" data-toggle="pill" href="#pills-User" role="tab" aria-controls="pills-User" aria-selected="true">User</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-Role-tab" data-toggle="pill" href="#pills-Role" role="tab" aria-controls="pills-Role" aria-selected="false">Role</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-User" role="tabpanel" aria-labelledby="pills-User-tab"><?php require_once 'UserList.php' ?></div>
                <div class="tab-pane fade" id="pills-Role" role="tabpanel" aria-labelledby="pills-Role-tab"><?php require_once 'RoleList.php' ?></div>
            </div>
        </div>
    </div>
</div>
<?php require_once 'Footer.php' ?>