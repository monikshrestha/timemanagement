<?php require_once 'Header.php' ?>

 <div class="Login">
      <div class="row body">
        <div class="col">
          <div class="container">
            <div class="page-informaton">
              <h4>Welcome to taskmanangement <br />system</h4>
              <p>
                We are here to improve and record all the daily task of yours
              </p>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="login-wrapper">
            <div class="logincard">
              <form>
                <div class="formlabel">
                  <p>Registration form</p>
                </div>
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label> First name</label>
                      <input type="text" class="form-control" />
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label> Last name</label>
                      <input type="text" class="form-control" />
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label> Address</label>
                      <input type="text" class="form-control" />
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label> Phone-Number</label>
                      <input type="text" class="form-control" />
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label> Password</label>
                      <input type="text" class="form-control" />
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label> Confirm Password</label>
                      <input type="text" class="form-control" />
                    </div>
                  </div>
                </div>
                <input
                  type="submit"
                  value="signup"
                  class="btn btn-outline-success mt-3 shadow-sm font-weight-bold"
                />
                <div class="signup">
                  Already have an acoount 
                  <span>
                    <a href="Loginpage.php"> Login </a>
                  </span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php require_once 'Footer.php' ?>