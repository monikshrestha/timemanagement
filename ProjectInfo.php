<?php require_once 'Header.php' ?>

<div class="row body">
  <div class="col-2">
    <?php require_once 'Sidebar.php' ?>
  </div>
  <div class="col-10">
    <div class="mainbody">
      <div class="topic-head">
        <div class="topic-left">
        <a href="http://localhost/timemanagement/ProjectList.php"><i class="fas fa-angle-left"></i></a> Project Info
        </div>
      </div>

      <div class="project-det">
        <div class="form-group row">
          <div class="form-group col-md-4">
            <label for="formGroupExampleInput">Project Name</label>
            <input class="form-control" type="text" placeholder="Project Name" readonly>
          </div>
        </div>

        <div class="form-group row">
          <div class="form-group col-md-10">
            <label for="exampleFormControlTextarea1">Project Description</label>
            <input class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>
          </div>
        </div>
      </div>


      <div class="client-det">
        <div class="sub-topic">
          Client Information
        </div>
        <div class="row">

          <div class="form-group col-md-4">
            <label for="formGroupExampleInput">Client Name</label>
            <input class="form-control" type="text" placeholder="Client Name" readonly>
          </div>

          <div class="form-group col-md-4">
            <label for="formGroupExampleInput">Client Organization</label>
            <input class="form-control" type="text" placeholder="Client Organization" readonly>
          </div>

          <div class="form-group col-md-4">
            <label for="formGroupExampleInput">Client Address</label>
            <input class="form-control" type="text" placeholder="Client Address" readonly>
          </div>

          <div class="form-group col-md-4">
            <label for="formGroupExampleInput">Client Phone No.</label>
            <input class="form-control" type="text" placeholder="Client Phone No." readonly>
          </div>
        </div>
      </div>

      <div class="server-det">
        <div class="row">
          <div class="col-sm">

            <div class="sub-topic">
              Server Information
            </div>
            <div class="form-group row">
              <label for="staticValue" class="col-sm-4 col-form-label">Server URL</label>
              <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="staticValue" value="Server URL">
              </div>

              <label for="staticValue" class="col-sm-4 col-form-label">Server DNS</label>
              <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="staticValue" value="Server DNS">
              </div>

              <label for="staticValue" class="col-sm-4 col-form-label">Server Address</label>
              <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="staticValue" value="Server Address">
              </div>

              <label for="staticValue" class="col-sm-4 col-form-label">Server Version</label>
              <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="staticValue" value="Server Version">
              </div>

              <label for="staticValue" class="col-sm-4 col-form-label">Server Port</label>
              <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="staticValue" value="Server Port">
              </div>

            </div>
          </div>

          <div class="col-sm">
            <div class="sub-topic">
              Server Credentials
            </div>

            <div class="form-group row">
              <label for="staticValue" class="col-sm-4 col-form-label">Server Role</label>
              <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="staticValue" value="Server Role">
              </div>

              <label for="staticValue" class="col-sm-4 col-form-label">Server Username</label>
              <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="staticValue" value="Server Username">
              </div>

              <label for="staticValue" class="col-sm-4 col-form-label">Server Password</label>
              <div class="col-sm-6">
                <input type="text" readonly class="form-control-plaintext" id="staticValue" value="Server Password">
              </div>
            </div>
          </div>
        </div>

      </div>
      <a href="http://localhost/timemanagement/ProjectList.php"><button type="button" class="btn btn-info">Back To List</button></a>
    </div>
  </div>
</div>
</div>
<?php require_once 'Footer.php' ?>