<div class="col">

    <div class="topic-head">
        <div class="topic-left">
            User List
        </div>

        <div class="topic-right">
            <a href="http://localhost/timemanagement/UserAdd.php" class="add_button">Add Users</a>
        </div>
    </div>
    <!-- end of topic-head -->

    <div class="content-table">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">S.No.</th>
                    <th scope="col">User ID</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Role</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>User Name</td>
                    <td>Full Name</td>
                    <td>Super Admin</td>
                    <td>
                        <a href="http://localhost/timemanagement/UserEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                        <button type="button" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>User Name</td>
                    <td>Full Name</td>
                    <td>Project Manager</td>
                    <td>
                        <a href="http://localhost/timemanagement/UserEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                        <button type="button" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>User Name</td>
                    <td>Full Name</td>
                    <td>User</td>
                    <td>
                        <a href="http://localhost/timemanagement/UserEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                        <button type="button" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- div close content-table -->
</div>