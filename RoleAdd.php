<?php require_once 'Header.php' ?>

<div class="row body">
    <div class="col-2">
        <?php require_once 'Sidebar.php' ?>
    </div>
    <div class="col-10">
        <div class="mainbody">
            <div class="topic-head">
                <div class="topic-left">
                <a href="http://localhost/timemanagement/UserMgmt.php"><i class="fas fa-angle-left"></i></a> Add Role
                </div>
            </div>
            <!-- end of topic-head -->
            <form>
                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label for="formGroupExampleInput">Role ID</label>
                        <input class="form-control" type="text" placeholder="User Name">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="formGroupExampleInput">Role Name</label>
                        <input class="form-control" type="text" placeholder="User Name">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="form-group col-md-4">

                        <div class="role-mgmt">

                            <div class="sub-topic">
                                Role Management
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Dashboard
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Project
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Daily Task
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    User and Role
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Profile
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-danger">Cancel</button>
                <a href="http://localhost/timemanagement/UserMgmt.php"><button type="button" class="btn btn-info">Back To List</button></a>
            </form>

        </div>
    </div>
</div>
<?php require_once 'Footer.php' ?>