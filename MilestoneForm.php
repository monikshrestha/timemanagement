<?php require_once 'Header.php' ?>

<div class="row body">
  <div class="col-2">
    <?php require_once 'Sidebar.php' ?>
  </div>
  <div class="col-10">
    <div class="mainbody">
      <div class="container">
        <div class="formwrapper">
          <div class="MainHeading">
          <a href=""><i class="fas fa-chevron-left"></i></a>
          Milestone</div>
          <div class="row">
            <div class="col-5">
              <p>Task name</p>
              <input class="form-control" type="text" placeholder="" />
            </div>
            <div class="col-5">
              <p>Assign User</p>
              <form>
                <div class="multipleSelection">
                  <div class="selectBox" onclick="showCheckboxes()">
                    <select>
                      <option>User</option>
                    </select>
                    <div class="overSelect"></div>
                  </div>

                  <div id="checkBoxes">
                    <div class="row">
                      <div class="col-6">
                      <label for="first">
                      <input type="checkbox" id="first" />
                      User
                    </label>
                      </div>
                      <div class="col-6">
                      <label for="first">
                      <input type="checkbox" id="first" />
                      User
                    </label>
                      </div>
                      <div class="col-6">
                      <label for="first">
                      <input type="checkbox" id="first" />
                      User
                    </label>
                      </div>
                      <div class="col-6">
                      <label for="first">
                      <input type="checkbox" id="first" />
                      User
                    </label>
                      </div>
                    </div>
                  </div>
                </div>

              </form>
            </div>
            <div class="col-5">
              <p>Starting Date</p>
              <input type="text" name="Date" value="10/24/2000" />
            </div>
            <div class="col-5">
              <p>Ending</p>
              <input type="text" name="Date" value="10/24/2000" />
            </div>
            <div class="col-5">
              <p>Description</p>
              <textarea name="" id="description" cols="30" rows="5"></textarea>
            </div>
          </div>
          <button type="button" class="btn btn-success"> <i class="fas fa-save"></i> Save</button>
          <button type="button" class="btn btn-danger"><i class="fas fa-window-close"></i> cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once 'Footer.php' ?>
