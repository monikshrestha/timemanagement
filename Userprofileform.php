<?php require_once 'Header.php' ?>

<div class="row body">
  <div class="col-2">
    <?php require_once 'Sidebar.php' ?>
  </div>
  <div class="col-10">
    <div class="mainbody">
      <form>
        <input type="file" accept="image/*" onchange="loadFile(event)" />
        <img id="output" height="90px" width="90px" />
        <div class="userform">
          <div class="row">
            <div class="col-5">
              <div class="formgroup">
                <p>First name</p>
                <input type="text" class="form-control" />
              </div>
            </div>
            <div class="col-5">
              <div class="formgroup">
                <p>Last name</p>
                <input type="text" class="form-control" />
              </div>
            </div>
            <div class="col-5">
              <div class="formgroup">
                <p>User Name</p>
                <input type="text" class="form-control" />
              </div>
            </div>
            <div class="col-5">
              <div class="formgroup">
                <p>CompanyName</p>
                <input type="text" class="form-control" />
              </div>
            </div>
            <div class="col-5">
              <div class="formgroup">
                <p>Password</p>
                <input type="text" class="form-control" />
              </div>
            </div>
            <div class="col-5">
              <div class="formgroup">
                <p>Confirm Password</p>
                <input type="text" class="form-control" />
              </div>
            </div>
            <div class="col-5">
              <div class="formgroup">
                <p>Description</p>
                <textarea
                  class="form-control"
                  rows="5"
                  placeholder="Description"
                >
                </textarea>
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-danger">Cancel</button>
        </div>
      </form>
    </div>
  </div>
</div>


<?php require_once 'Footer.php' ?>