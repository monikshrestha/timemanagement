<div class="col">
  <div class="topic-head">
        <div class="topic-left">
            Role List
        </div>

        <div class="topic-right">
            <a href="http://localhost/timemanagement/RoleAdd.php" class="add_button">Add Role</a>
        </div>
    </div>
    <!-- end of topic-head -->
  <div class="content-table">
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">S.No.</th>
          <th scope="col">Role ID</th>
          <th scope="col">Role Name</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Role ID</td>
          <td>Role Name</td>
          <td>
            <a href="http://localhost/timemanagement/RoleEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
            <button type="button" class="btn btn-danger btn-sm">Delete</button>
          </td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Role ID</td>
          <td>Role Name</td>
          <td>
            <a href="http://localhost/timemanagement/RoleEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
            <button type="button" class="btn btn-danger btn-sm">Delete</button>
          </td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>Role ID</td>
          <td>Role Name</td>
          <td>
            <a href="http://localhost/timemanagement/RoleEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
            <button type="button" class="btn btn-danger btn-sm">Delete</button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- div close content-table -->
</div>