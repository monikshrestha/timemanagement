<?php require_once 'Header.php' ?>
  <div class="Login">
    <div class="row body">
      <div class="col">
        <div class="container">
          <div class="page-informaton">
            <h4>Welcome to taskmanangement <br>system</h4>
            <p>We are here to improve and record all the daily task of yours</p>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="login-wrapper">
          <div class="logincard">
            <form>
              <div class="formlabel">
                <p>LOGIN PANEL</p>
              </div>
              <div class="form-group">
                <i class="fas fa-user"></i><label for="email" class="pl-2 font-weight-bold">Email</label><input type="text" class="form-control" placeholder="Email" name="rEmail" />
              </div>
              <div class="form-group">
                <i class="fas fa-key"></i><label for="pass" class="pl-2 font-weight-bold">Password</label><input type="text" class="form-control" placeholder="Password" name="rPassword" />
              </div>
              <input type="submit" value="Login" class="btn btn-outline-success mt-3 shadow-sm font-weight-bold" />
              <div class="signup">
                Dont have an account?
                <span>
                  <a href="RegistrationPage.php"> Signup </a>
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php require_once 'Footer.php' ?>
