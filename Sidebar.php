<div class="sidebar">
  <div class="name">
    Task Manangement System
  </div>
  <div class="sidebar-menu">
    <nav>
    <ul>
      <li><a href="http://localhost/timemanagement/Dashboard.php"><i class="fas fa-chart-line"></i> Dashboard</a></li>
      <li><a href="http://localhost/timemanagement/Dailytask.php"><i class="fas fa-project-diagram"></i> Daily task</a></li>
      <li><a href="http://localhost/timemanagement/Maintask.php"><i class="fas fa-project-diagram"></i> Main Task</a></li>
      <li><a href="http://localhost/timemanagement/Milestones.php"><i class="fas fa-project-diagram"></i> Milestone</a></li>
      <li>
        <a href="#" class="set-btn">
        <i class="fas fa-wrench"></i> Setting <p id=down class="fas fa-caret-down"></p>
        </a>
        <ul class="sub-menu">
          <li><a href="http://localhost/timemanagement/ProjectList.php"><i class="fas fa-tasks"></i> Project</a></li>
          <li><a href="http://localhost/timemanagement/UserMgmt.php"><i class="fas fa-user"></i> User & Roles</a></li>
        </ul>
      </li>
      <li><a href="http://localhost/timemanagement/Userprofile.php"> <i class="fas fa-user"></i> Profile</a></li>
    </ul>
    </nav>
  </div>
</div>