<?php require_once 'Header.php' ?>

<div class="row body">
  <div class="col-2">
    <?php require_once 'Sidebar.php' ?>
  </div>
  <div class="col-10">
    <div class="mainbody">
      <div class="Heading">
        Overview
      </div>
      <div class="row">
        <div class="col-4">
          <div class="card card1">
            <div class="cardheading">
             <h6> User </h6>
             <p> <i class="fas fa-circle" style="color: #11ff00"></i> Active User=1</p> 
            </div>
            <p>Admin user count</p>
            <p>Normal User count</p>
            <p>Total User count</p>
          </div>
        </div>
        <div class="col-4">
          <div class="card card1">
          <div class="cardheading">
             <h6> Running Project </h6>
             <p> <i class="fas fa-circle" style="color: #11ff00"></i> Active Project=1</p> 
            </div>
            <p>Remaining Project count</p>
            <p>Project Completed Count</p>
            <p>Total Project count</p>
          </div>
          </div>
        <div class="col-4">
        <div class="card card1">
          <div class="cardheading">
             <h6> Project title </h6>
            </div>
            <p>Remaining task count</p>
            <p>Task Completed Count</p>
            <p>Total Task count</p>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require_once 'Footer.php' ?>