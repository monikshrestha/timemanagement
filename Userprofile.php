<?php require_once 'Header.php' ?>
<div class="row body">
  <div class="col-2">
    <?php require_once 'Sidebar.php' ?>
  </div>
  <div class="col-10">
    <div class="mainbody">
      <div class="UserProfile">
        <div class="logout">
          <a href="LoginPage.php"
            ><i class="fas fa-sign-out-alt"></i> Log-out</a
          >
        </div>
        <img src="./image/Background.jpg" height="90px" width="90px" />
        <p>Kshitiz Moktan Tamang</p>
        <span> @Username</span>
      </div>
      <div class="UserDetails">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a
              class="nav-link active"
              id="Task-Tab"
              data-toggle="tab"
              href="#Task"
              role="tab"
              aria-controls="home"
              aria-selected="true"
              >Project</a
            >
          </li>
          <li class="nav-item">
            <a
              class="nav-link"
              id="profile-tab"
              data-toggle="tab"
              href="#profile"
              role="tab"
              aria-controls="profile"
              aria-selected="false"
              >Profile</a
            >
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div
            class="tab-pane fade show active"
            id="Task"
            role="tabpanel"
            aria-labelledby="Task-tab"
          >
            <?php require_once 'UserProject.php' ?>
          </div>
          <div
            class="tab-pane fade"
            id="profile"
            role="tabpanel"
            aria-labelledby="profile-tab"
          >
            <?php require_once 'UserDetails.php' ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once 'Footer.php' ?>