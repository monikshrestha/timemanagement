
 <div class="task">

  <div class="content-table">
          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">S.No.</th>
                <th scope="col">Projects</th>
                <th scope="col">Client</th>
                <th scope="col">Member</th>
                <th scope="col">Time(In Days)</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td><a href="ProjectInfo.php">Project Name</a></td>
                <td>Project Client</td>
                <td>10</td>
                <td>60</td>
                <td>
                  <a href="http://localhost/timemanagement/ProjectEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                  <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td><a href="ProjectInfo.php">Project Name</a></td>
                <td>Project Client</td>
                <td>5</td>
                <td>70</td>
                <td>
                  <a href="http://localhost/timemanagement/ProjectEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                  <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td><a href="ProjectInfo.php">Project Name</a></td>
                <td>Project Client</td>
                <td>7</td>
                <td>40</td>
                <td>
                  <a href="http://localhost/timemanagement/ProjectEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                  <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </td>
              </tr>
            </tbody>
          </table>
  
  
        </div>
</div>