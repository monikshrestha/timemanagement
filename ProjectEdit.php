<?php require_once 'Header.php' ?>

<div class="row">
    <div class="col-2">
        <?php require_once 'Sidebar.php' ?>
    </div>
    <div class="col-10">


        <div class="mainbody">

            <div class="topic-head">
                <div class="topic-left">
                <a href="http://localhost/timemanagement/ProjectList.php"><i class="fas fa-angle-left"></i></a> Edit Project
                </div>
            </div>
            <!-- end of topic-head -->
            <form>
                <div class="form-group row">
                    <div class="form-group col-md-6">
                        <label for="formGroupExampleInput">Project Name</label>
                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Project Description</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <div class="form-group row">
                <div class="form-group col-md-4">
                         <label for="formGroupExampleInput">Assign User</label>
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Assign User</option>
                            <option value="1">User One</option>
                            <option value="2">User Two</option>
                            <option value="3">User Three</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label for="formGroupExampleInput">Project To- From</label>
                        <input type="text" class="form-control" name="datefilter" value="" />
                    </div>

                </div>
                <div class="project-range">

                </div>

                <div class="client-credentials">
                    <div class="sub-topic">
                        Client Details
                    </div>
                    <div class="form-group row">
                        <div class="form-group col-md-4">
                            <label for="inputClientName">Client Name</label>
                            <input type="text" class="form-control" id="inputClientName" placeholder="Client Name">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputClientOrg">Client Organization</label>
                            <input type="text" class="form-control" id="inputClientOrg" placeholder="Client Organization">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputClentAdd">Client Address</label>
                            <input type="text" class="form-control" id="inputClentAdd" placeholder="Client Address">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputClentNum">Client Number</label>
                            <input type="text" class="form-control" id="inputClentNum" placeholder="Client Number">
                        </div>
                    </div>
                </div>

                <div class="user-credentials">
                    <div class="sub-topic">
                        User Credentials
                    </div>
                    <div class="form-group row">
                        <div class="form-group col-md-4">
                            <label for="inputUsername">Username</label>
                            <input type="text" class="form-control" id="inputUsername" placeholder="name">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputPassword">Password</label>
                            <input type="text" class="form-control" id="inputPassword" placeholder="Password">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="inputUserRole">User Role</label>
                            <input type="text" class="form-control" id="inputUserRole" placeholder="User Role">
                        </div>

                        <div class="form-group col-md-4">
                            <button type="submit" class="btn btn-primary">Add More</button>
                        </div>
                    </div>

                </div>
                <div class="server-credentials">
                    <div class="sub-topic">
                        Server Credentials
                    </div>
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="inputUsername">Username</label>
                            <input type="text" class="form-control" id="inputUsername" placeholder="name">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputPassword">Password</label>
                            <input type="text" class="form-control" id="inputPassword" placeholder="Password">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputServerURL">Server URL</label>
                            <input type="text" class="form-control" id="inputServerURL" placeholder="Server URL">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputServerDNS">Server DNS</label>
                            <input type="text" class="form-control" id="inputServerDNS" placeholder="Server DNS">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputServerAddress">Server Address</label>
                            <input type="text" class="form-control" id="inputServerAddress" placeholder="Server Address">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputServerVersion">Server Version</label>
                            <input type="text" class="form-control" id="inputServerVersion" placeholder="Server Version">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputServerPort">Server Port</label>
                            <input type="text" class="form-control" id="inputServerPort" placeholder="Server Port">
                        </div>
                    </div>
                </div>


                <button type="button" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-danger">Cancel</button>
                <a href="http://localhost/timemanagement/ProjectList.php"><button type="button" class="btn btn-info">Back To List</button></a>
            </form>

        </div>
    </div>
</div>
<?php require_once 'Footer.php' ?>