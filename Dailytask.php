<?php require_once 'header.php' ?>

<div class="row body">
  <div class="col-2">
    <?php require_once 'Sidebar.php' ?>
  </div>
  <div class="col-10">
    <div class="mainbody">

      <div class="topic-head">
        <div class="topic-left">
         Daily Task
        </div>

        <div class="topic-right">
          <a href="Subtask.php" class="add_button">Add task</a>
        </div>
      </div>
    
      <div id="calendar"></div>
        </div>
  
    </div>
  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>

<!-- Add the evo-calendar.js for.. obviously, functionality! -->
<script src="https://cdn.jsdelivr.net/npm/evo-calendar@1.1.2/evo-calendar/js/evo-calendar.min.js"></script>
<script>
  $("#calendar").evoCalendar({
    theme: 'Royal Navy'
  });
</script>
<?php require_once 'footer.php' ?>