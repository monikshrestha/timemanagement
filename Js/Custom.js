var show = true; 
  
function showCheckboxes() { 
    var checkboxes =  
        document.getElementById("checkBoxes"); 

    if (show) { 
        checkboxes.style.display = "block"; 
        show = false; 
    } else { 
        checkboxes.style.display = "none"; 
        show = true; 
    } 
} 

$(function() {
  $('input[name="Date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 2000,
    maxYear: parseInt(moment().format('YYYY'),10)
  });
});

$('input[name="datefilter"]').daterangepicker({
    "timePicker": true,
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    "alwaysShowCalendars": true,
    "startDate": "02/12/2021",
    "endDate": "02/18/2021"
}, function(start, end, label) {
  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
});

var loadFile = function(event) {
  var output = document.getElementById('output');
  output.src = URL.createObjectURL(event.target.files[0]);
  output.onload = function() {
    URL.revokeObjectURL(output.src) // free memory
  }
};

// on-click sidebar dropdown
$('.set-btn').click(function(){
  $('nav ul .sub-menu').toggleClass("show");
});

// on-click active class
$(document).ready(function () {
  var current = location.pathname;
  $('nav ul li a').each(function(){
      var $this = $(this);
      // if the current path is like this link, make it active
      if($this.attr('href').indexOf(current) !== -1){
          $this.addClass('active');
      }
  })
});



