<?php require_once 'header.php' ?>

<div class="row body">
  <div class="col-2">
    <?php require_once 'Sidebar.php' ?>
  </div>
  <div class="col-10">
    <div class="mainbody">
      <div class="container">

        <div class="topic-head">
          <div class="topic-left">
            Projects
          </div>
  
          <div class="topic-right">
            <a href="http://localhost/timemanagement/ProjectAdd.php" class="add_button">Add Project</a>
          </div>
        </div>
        <!-- end of topic-head -->
  
        <div class="content-table">
          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">S.No.</th>
                <th scope="col">Projects</th>
                <th scope="col">Client</th>
                <th scope="col">Member</th>
                <th scope="col">Time(In Days)</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td><a href="ProjectInfo.php">Project Name</a></td>
                <td>Project Client</td>
                <td>10</td>
                <td>60</td>
                <td>
                  <a href="http://localhost/timemanagement/ProjectEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                  <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td><a href="ProjectInfo.php">Project Name</a></td>
                <td>Project Client</td>
                <td>5</td>
                <td>70</td>
                <td>
                  <a href="http://localhost/timemanagement/ProjectEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                  <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </td>
              </tr>
              <tr>
                <th scope="row">3</th>
                <td><a href="ProjectInfo.php">Project Name</a></td>
                <td>Project Client</td>
                <td>7</td>
                <td>40</td>
                <td>
                  <a href="http://localhost/timemanagement/ProjectEdit.php"><button type="button" class="btn btn-warning btn-sm">Edit</button></a>
                  <button type="button" class="btn btn-danger btn-sm">Delete</button>
                </td>
              </tr>
            </tbody>
          </table>
  
  
        </div>
        <!-- content-table -->
      </div>
    </div>
      </div>
</div>

<?php require_once 'footer.php' ?>