<?php require_once 'Header.php' ?>

<div class="row body">
  <div class="col-2">
    <?php require_once 'Sidebar.php' ?>
  </div>
  <div class="col-10">
    <div class="mainbody">
      <div class="container">
        <div class="formwrapper">
          <div class="MainHeading">Sub task</div>
          <div class="row">
            <div class="col-5">
              <p>Task name</p>
              <input class="form-control" type="text" placeholder="" />
            </div>
            <div class="col-5">
              <p>Task</p>
              <div class="selectBox" >
                    <select class="form-select" aria-label="Default select example">
                      <option>select task</option>
                      <option value="1">User One</option>
                            <option value="2">User Two</option>
                            <option value="3">User Three</option>
                    </select>
                  </div>
            </div>
            <div class="col-3">
              <p>Starting Date</p>
              <input type="text" name="Date" value="10/24/2000" />
            </div>
            <div class="col-3">
              <p>Starting Time</p>
              <input class="clocklet-scroll-into-view" type="text" data-clocklet maxlength="5" value="01:23">
            </div>
            <div class="col-3">
              <p>Ending Time</p>
              <input class="clocklet-scroll-into-view" type="text" data-clocklet maxlength="5" value="01:23">
            </div>
            <div class="col-5">
              <p>Description</p>
              <textarea name="" id="description" cols="30" rows="5"></textarea>
            </div>
          </div>
          <button type="button" class="btn btn-success"> <i class="fas fa-save"></i> Save</button>
          <button type="button" class="btn btn-danger"><i class="fas fa-window-close"></i> cancel</button>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once 'Footer.php' ?>
