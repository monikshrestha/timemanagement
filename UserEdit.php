<?php require_once 'Header.php' ?>

<div class="row body">
    <div class="col-2">
        <?php require_once 'Sidebar.php' ?>
    </div>
    <div class="col-10">
        <div class="mainbody">
            <div class="topic-head">
                <div class="topic-left">
                <a href="http://localhost/timemanagement/UserMgmt.php"><i class="fas fa-angle-left"></i></a> Edit User
                </div>
            </div>
            <!-- end of topic-head -->
            <form>

                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label for="formGroupExampleInput">User Name</label>
                        <input class="form-control" type="text" placeholder="User Name">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="formGroupExampleInput">User Password</label>
                        <input class="form-control" type="text" placeholder="User Password" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label for="formGroupExampleInput">Full Name</label>
                        <input class="form-control" type="text" placeholder="Full Name">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="formGroupExampleInput">User Phone No.</label>
                        <input class="form-control" type="text" placeholder="User Phone No.">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="formGroupExampleInput">User Email</label>
                        <input class="form-control" type="text" placeholder="User Email">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <div class="role-mgmt">
                            <div class="sub-topic">
                                Role Management
                            </div>

                            <select class="form-select" aria-label="Default select example">
                                <option selected>Select Roles</option>
                                <option value="1">Role One</option>
                                <option value="2">Role Two</option>
                                <option value="3">Role Three</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <div class="proj-mgmt">
                                <div class="sub-topic">
                                    Project Management
                                </div>

                                <select class="form-select" aria-label="Default select example">
                                    <option selected>Select Project</option>
                                    <option value="1">Project One</option>
                                    <option value="2">Project Two</option>
                                    <option value="3">Project Three</option>
                                </select>
                            </div>
                        </div>
                    </div>


            </form>


        </div>
        <button type="button" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger">Cancel</button>
    </div>
</div>

<?php require_once 'Footer.php' ?>